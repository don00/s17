/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getPersonalInfo() {
		let fullName = prompt("Enter you full name: ");
		console.log('Hello ' + fullName);
		let age = prompt("Enter you age:");
		console.log('You are ' + age + " years old.");
		let location = prompt("Enter you address:" );
		console.log('You live in ' + location);
		alert('Thank you for the input');
	}

	getPersonalInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printFavoriteBands(){
		let favoriteBands = [ ,  , , , ]
		console.log('1. Linkin park');
		console.log('2. Avril lavigne' );
		console.log('3. Air Supply' );
		console.log('4. Five Finger Death Punch');
		console.log('5. SimplePlan');

	};

	printFavoriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function printFavoriteMovies(){
		console.log('1. Pacific Rim');
		console.log('Rotten Tomatoes Rating: 72%');
		console.log('2. Avengers Endgame');
		console.log('Rotten Tomatoes Rating: 94%' );
		console.log('3. Schindler\'s List');
		console.log('Rotten Tomatoes Rating: 98% ');
		console.log('4. Shawshank Redemption');
		console.log('Rotten Tomatoes Rating: 91%');
		console.log('5. Kung Fu Panda');
		console.log('Rotten Tomatoes Rating: 87%');

	}

	printFavoriteMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

